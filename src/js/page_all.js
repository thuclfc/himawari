$(document).ready(function () {
    //navbar
    $('.navbar-toggler').click(function () {
        $('header nav').toggleClass('active');
        $('.modal-menu').addClass('is-open');
    });
    $('.navbar-collapse .close,.modal-menu').click(function(){
        $(".navbar-collapse").collapse('hide');
        $('.navbar').removeClass('active');
        $('.modal-menu').removeClass('is-open');
    });

    $(window).on("load", function (e) {
        $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'><i class='fas fa-sort-down'></i></span>");
    });

    var url_current = window.location.href;
    $('.navbar-nav li a[href$="'+url_current+'"]').addClass('active');

});