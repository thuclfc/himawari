$(document).ready(function () {
    $('.video_bg').on('click',function(){
        var data_video = $(this).data('video');
        $('.popup-video iframe').attr('src',data_video);
    });
    $('.popup-video .close,.modal').on('click',function(){
        $('.popup-video iframe').attr('src','');
    });

    $('.slider_himawari .slider').slick({
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        fade: true,
        dots: false,
        arrows: true,
    });
});