$(document).ready(function () {
    tabs('.program_tab li','.program_content',0,0);
    $('.menu_list').slick({
        centerMode: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        rows: 2,
        infinite: false,
        centerPadding: 0,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                }
            }
        ]
    });

    function tabs(tab,tab_content,tab_active,index_active) {
        $(tab_content).addClass('hidden_tab');
        $(tab).eq(tab_active).addClass('active');
        $(tab_content).eq(index_active).removeClass('hidden_tab').addClass('show_tab');
        $(tab).click(function () {
            $(tab).removeClass('active');
            $(this).addClass('active');
            var tab_index=$(this).index();
            $(tab_content).addClass('hidden_tab').removeClass('show_tab');
            $(tab_content).eq(tab_index).removeClass('hidden_tab').addClass('show_tab');
        });
    }
});